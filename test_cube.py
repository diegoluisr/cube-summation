import unittest
import cube
import os

INPUT_DATA = os.path.join(os.path.dirname(__file__), 'data/input')
OUTPUT_DATA = os.path.join(os.path.dirname(__file__), 'data/output')

class TestCube(unittest.TestCase):
    def setUp(self):
        self.indata = open(INPUT_DATA).read().split("\n")
        self.outdata = open(OUTPUT_DATA).read().split("\n")

    def testFromData(self):
        # with self.assertRaises(Exception):
        line = 0
        tests = int(self.indata[line])
        line += 1
        if tests < 1 or tests > 50:
            raise Exception('T < 1 or T > 50')

        for i in range(0, tests):
            transactions = []
            n, m = self.indata[line].split(' ')
            line += 1
            endline = line+int(m)
            for j in range(line, endline):
                if self.indata[j][:3] == 'UPD' or self.indata[j][:3] == 'QUE':
                    line += 1
                    transactions.append(self.indata[j])

            if int(m) == len(transactions):
                self.__singleTest(int(n), int(m), transactions)


    def __singleTest(self, n, m, transactions):
        if len(transactions) < 1 or len(transactions) > 1000:
            raise Exception('M < 1 or M > 1000')

        matrix = cube.Cube(n)

        for transaction in transactions:
            if len(transaction) > 5:
                items = transaction.split(' ')
                if items[0] == 'UPDATE' and len(items) == 5:
                    matrix.update(
                        int(items[1]), 
                        int(items[2]), 
                        int(items[3]), 
                        int(items[4]))
                elif items[0] == 'QUERY' and len(items) == 7:
                    result = matrix.query(int(items[1]), int(items[2]), int(items[3]), int(items[4]), int(items[5]), int(items[6]))
                    if len(self.outdata) > 0:
                        self.assertEqual(int(self.outdata[0]), result, 'OUTPUT NOT MATCH')
                        del self.outdata[0]


if __name__ == '__main__':
    unittest.main()