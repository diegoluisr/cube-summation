
# Algunas definiciones encontradas:
# https://en.wikipedia.org/wiki/Fenwick_tree

# Soluciones encontradas usadas como referencia:
# http://allhackerranksolutions.blogspot.com.co/2016/08/cube-summation-hacker-rank-solution.html
# https://github.com/MiguelCrespo/cube_summation/blob/master/app/Matrix.php

class Cube():

    matrix = []

    def __init__(self, size):
        matrix = []
        if size > 100 or size < 1:
            raise Exception('N > 100 or N < 1')
        for i in range(0, size):
            matrix.append([])
            for j in range(0, size):
                matrix[i].append([])
                for k in range(0, size):
                    matrix[i][j].append(0)
        self.matrix = matrix


    def update (self, x, y, z, value):
        size = len(self.matrix)
        if x > size or y > size or z > size:
            raise Exception('x, y or z > size')
        if x < 1 or y < 1 or z < 1:
            raise Exception('x, y or z < 1')
        if value < -1000000000 or value > 1000000000:
            raise  Exception('W > -10e9 or W > 10e9')
        self.matrix[x-1][y-1][z-1] = value

    def query(self, x1, y1, z1, x2, y2, z2):
        if x1 < 1 or y1 < 1 or z1 < 1:
            raise Exception('x1 < 1 or y1 < 1 or z1 < 1')
        if x1 > x2 or y1 > y2 or z1 > z2:
            raise Exception('x1 > x2 or y1 > y2 or z1 > z2')

        sum = 0
        for i in range(x1-1, x2):
            for j in range(y1-1, y2):
                for k in range(z1-1, z2):
                    sum = sum + self.matrix[i][j][k]
        return sum
